<div class="home_content">
        <div class="page_title">
            <h3>Feature data</h3>
        </div>
        <div class="apps_outer">
            <h4 class="sub_Title">Apps</h4>
            <div class="applist_inner">
                <ul class="applist d-flex flex-wrap"></ul>
            </div>
        </div>
        <div class="workspace_outer mt-20">
            <h4 class="sub_Title">Workspace</h4>
            <div class="workspace_list">
                <ul class="workspace_tab"></ul>
            </div>
        </div>
    </div>